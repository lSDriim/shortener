const { expect } = require('chai')
const request = require('supertest')
const cheerio = require('cheerio')
const app = require('../src/app.js')
const Model = require('../src/model')

describe('Checking creating of new short url', () => {
  it('should show error if there no url', (done) => {
    request(app)
      .post('/')
      .expect(400, done)
  })

  it('should show error if there some thing other then url', (done) => {
    request(app)
      .post('/')
      .send('url=testtest')
      .expect(400, done)
  })

  it('should create new short url and cookie with new uid', (done) => {
    request(app)
      .post('/')
      .send('url=test.url')
      .expect(200)
      .then((res) => {
        const $ = cheerio.load(res.text)
        const short = $('a').text().match(/http:\/\/localhost:3000\/(.+)/)
        return Model.findOne({ short: short[1] })
      })
      .then((result) => {
        expect(result).to.be.not.equal(null)

        done()
      })
  })
  it('should create new short with existing cookie', (done) => {
    request(app)
      .post('/')
      .set('Cookie', 'uid=1234567')
      .send('url=test2.url')
      .expect(200)
      .then((res) => {
        return Model.findOne({ uid: '1234567' })
      })
      .then((result) => {
        expect(result).to.be.not.equal(null)
        expect(result.url).to.be.equal('test2.url')

        done()
      })
  })
})

describe('Checking accesing short url', () => {
  const shortUrl = 'asdbser'
  const url = 'google.com'
  const uid = '1234567'

  beforeEach((done) => {
    new Model({
      short: shortUrl,
      url: url,
      uid: uid
      /* count default 0 */
    }).save(() => done())
  })

  it('should return error if can not find short in db', (done) => {
    request(app)
      .get('/1234567')
      .expect(404, done)
  })

  it('should redirect if short is in db and count redirects', (done) => {
    request(app)
      .get(`/${shortUrl}`)
      .expect(302)
      .then((res) => {
        return Model.findOne({ uid: uid })
      })
      .then((result) => {
        expect(result.count).to.be.equal(1)
        done()
      })
  })
})
