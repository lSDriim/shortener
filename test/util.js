const mongoose = require('mongoose')

const opts = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
}

before((done) => {
  mongoose.connect('mongodb://localhost:27017/shortener_test', opts)
  mongoose.connection
    .once('open', () => { done() })
    .on('error', (error) => { console.warn('Warning', error) })
})

beforeEach((done) => {
  const { shorts } = mongoose.connection.collections
  shorts.drop(() => done())
})

after((done) => {
  mongoose.connection.close(() => done())
})
