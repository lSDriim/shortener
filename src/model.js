const { Schema, model } = require('mongoose')

const shortSchema = Schema({
  short: { type: String, unique: true },
  url: String,
  uid: String,
  count: { type: Number, default: 0 }
})

module.exports = model('Short', shortSchema)
