const mongoose = require('mongoose')
const nocache = require('nocache')
const app = require('./app.js')

const PORT = process.env.PORT || 3000
const DB = process.env.DB || 'mongodb://localhost:27017/shortener'

const opts = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
}

mongoose.Promise = global.Promise
mongoose.connect(DB, opts)

const db = mongoose.connection

db.on('error', (err) => {
  console.log(`error ocured: ${JSON.stringify(err)}`)
})
db.once('open', (callback) => {
  console.log('DB is opened')
})

db.on('disconnected', function () {
  console.log('Mongoose default connection disconnected')
  process.exit(-1)
})

/* disable cache to get count of redirects */
app.use(nocache())

app.listen(PORT, () => {
  console.log('Example app listening on port 3000!')
})
