const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const uniqid = require('uniqid')
const path = require('path')
const ShortUrl = require('./model')
const app = express()

const PORT = process.env.PORT || 3000
const SITE = process.env.SITE || 'http://localhost'

const SITE_URL = SITE + ':' + PORT

app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.get('/', (req, res) => {
  res.render('index')
})

app.post('/', (req, res) => {
  let uid = req.cookies.uid
  const url = req.body.url
  const reg = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g

  if (url === undefined || url.match(reg) === null) {
    res.status(400).render('index', { message: 'Not Found' })
    return
  }

  if (uid === undefined) {
    uid = uniqid() /* generate uniq 18 bytes user id */
    res.cookie('uid', uid)
  }

  const short = uniqid.time() /* generate uniq 8 bytes short url */

  new ShortUrl({
    short: short,
    url: url,
    uid: uid
    /* count default 0 */
  }).save((result) => {
    res.render('index', { url: `${SITE_URL}/${short}` })
  })
})

app.get('/:page', (req, res) => {
  const url = req.params.page

  ShortUrl
    .findOneAndUpdate({ short: url }, { $inc: { count: 1 } })
    .then((result) => {
      if (result === null) {
        res.status(404).render('index', { message: 'Not Found' })
        return
      }

      res.redirect(302, result.url)
    })
    .catch((error) => {
      console.log(error)
      res.status(500).render('index', { message: 'Internal Server Error' })
    })
})

module.exports = app
